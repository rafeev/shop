$(document).ready(function() {

	$('.left-menu__item__link-parent').click(function() {
		var menuHeight = $(this).next().height(),
			menuParent = $(this).parent().height()
		$('.left-menu__item').removeAttr('style')
		$('.left-menu-inner').removeClass('left-menu-inner-open')
		if (menuParent > 42) {
			$(this).parent().removeAttr('style')
			$(this).next().removeClass('left-menu-inner-open')
		} else {
			$(this).parent().height(menuHeight);
			$(this).next().addClass('left-menu-inner-open')
		}
	});

	$('.show-menu').click(function() {
		$('.header__menu').toggleClass('header__menu-open');
		$('body').toggleClass('overflow');
	});

	

});